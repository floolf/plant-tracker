<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191122141252 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE piece ADD relation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B233256915B FOREIGN KEY (relation_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_44CA0B233256915B ON piece (relation_id)');
        $this->addSql('ALTER TABLE plante ADD relation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plante ADD CONSTRAINT FK_517A69473256915B FOREIGN KEY (relation_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_517A69473256915B ON plante (relation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE relation');
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B233256915B');
        $this->addSql('DROP INDEX IDX_44CA0B233256915B ON piece');
        $this->addSql('ALTER TABLE piece DROP relation_id');
        $this->addSql('ALTER TABLE plante DROP FOREIGN KEY FK_517A69473256915B');
        $this->addSql('DROP INDEX IDX_517A69473256915B ON plante');
        $this->addSql('ALTER TABLE plante DROP relation_id');
    }
}
