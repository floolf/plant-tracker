<?php

namespace App\Form;

use App\Entity\Plante;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddPlanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Espece', TextType::class)
            ->add('Variete', TextType::class)
            ->add('DatePlantation',DateType::class, [
                'input'  => 'datetime_immutable'
            ])
            ->add('SaisonPlantation', ChoiceType::class,[
                'choices' => [
                    'Printemps' => 'printemps',
                    'Été' => 'été',
                    'Automne' => 'Automne',
                    'Hiver' => 'hiver',
                ]
            ])
            ->add('SaisonFloraison', ChoiceType::class,[
                'choices' => [
                    'Printemps' => 'printemps',
                    'Été' => 'été',
                    'Automne' => 'Automne',
                    'Hiver' => 'hiver',
                ]
            ])
            ->add('DateGermination', DateType::class, [
                'input'  => 'datetime_immutable'
            ])
            ->add('SaisonGermination', ChoiceType::class,[
                'choices' => [
                    'Printemps' => 'printemps',
                    'Été' => 'été',
                    'Automne' => 'Automne',
                    'Hiver' => 'hiver',
                ]
            ])
            ->add('Type', ChoiceType::class,[
                'choices' => [
                    'Grimpante' => 'grimpante',
                    'Aquatique' => 'aquatique',
                    'Bulbe' => 'bulbe',
                    'Bosquet' => 'bosquet',
                    'Iterieur' => 'interieur',
                    'Exterieur' => 'exterieur',
                ]
            ])
            ->add('TypeSol', ChoiceType::class,[
                'choices' => [
                    'Argileux' => 'argileux',
                    'Calcaire' => 'calcaire',
                    'Sableux' => 'sableux',
                    'Humifere' => 'humifere',
                    'Tourbeux' => 'tourbeux',
                    'Silicieux' => 'silicieux',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Plante::class,
        ]);
    }
}
