<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PieceRepository")
 */
class Piece
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Oriantation;

    /**
     * @ORM\Column(type="integer")
     */
    private $Temperature;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Hygrometrie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="pieces")
     */
    private $relation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getOriantation(): ?string
    {
        return $this->Oriantation;
    }

    public function setOriantation(string $Oriantation): self
    {
        $this->Oriantation = $Oriantation;

        return $this;
    }

    public function getTemperature(): ?int
    {
        return $this->Temperature;
    }

    public function setTemperature(int $Temperature): self
    {
        $this->Temperature = $Temperature;

        return $this;
    }

    public function getHygrometrie(): ?int
    {
        return $this->Hygrometrie;
    }

    public function setHygrometrie(?int $Hygrometrie): self
    {
        $this->Hygrometrie = $Hygrometrie;

        return $this;
    }

    public function getRelation(): ?User
    {
        return $this->relation;
    }

    public function setRelation(?User $relation): self
    {
        $this->relation = $relation;

        return $this;
    }
}
