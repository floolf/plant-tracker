<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanteRepository")
 */
class Plante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Espece;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Variete;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $DatePlantation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $SaisonPlantation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $SaisonFloraison;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $DateGermination;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $SaisonGermination;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $TypeSol='';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="plantes")
     */
    private $relation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEspece(): ?string
    {
        return $this->Espece;
    }

    public function setEspece(string $Espece): self
    {
        $this->Espece = $Espece;

        return $this;
    }

    public function getVariete(): ?string
    {
        return $this->Variete;
    }

    public function setVariete(string $Variete): self
    {
        $this->Variete = $Variete;

        return $this;
    }

    public function getDatePlantation(): ?\DateTimeImmutable
    {
        return $this->DatePlantation;
    }

    public function setDatePlantation(\DateTimeImmutable $DatePlantation): self
    {
        $this->DatePlantation = $DatePlantation;

        return $this;
    }

    public function getSaisonPlantation(): ?string
    {
        return $this->SaisonPlantation;
    }

    public function setSaisonPlantation(string $SaisonPlantation): self
    {
        $this->SaisonPlantation = $SaisonPlantation;

        return $this;
    }

    public function getSaisonFloraison(): ?string
    {
        return $this->SaisonFloraison;
    }

    public function setSaisonFloraison(string $SaisonFloraison): self
    {
        $this->SaisonFloraison = $SaisonFloraison;

        return $this;
    }

    public function getDateGermination(): ?\DateTimeImmutable
    {
        return $this->DateGermination;
    }

    public function setDateGermination(\DateTimeImmutable $DateGermination): self
    {
        $this->DateGermination = $DateGermination;

        return $this;
    }

    public function getSaisonGermination(): ?string
    {
        return $this->SaisonGermination;
    }

    public function setSaisonGermination(string $SaisonGermination): self
    {
        $this->SaisonGermination = $SaisonGermination;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getTypeSol(): string
    {
        return $this->TypeSol;
    }

    public function setTypeSol(string $TypeSol): self
    {
        $this->TypeSol = $TypeSol;

        return $this;
    }

    public function getRelation(): User
    {
        return $this->relation;
    }

    public function setRelation(User $relation): self
    {
        $this->relation = $relation;

        return $this;
    }
}
