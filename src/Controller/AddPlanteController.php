<?php

namespace App\Controller;

use App\Entity\Plante;
use App\Form\AddPlanteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddPlanteController extends AbstractController
{
    /**
     * @Route("/addPlante", name="add-plante")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request): Response
    {
        $plante = new Plante();
        $form = $this->createForm(AddPlanteType::class, $plante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($plante);
            $entityManager->flush();

            $this->addFlash('notice', 'Plante ajouter !'
            );

            //pour la redirection, ne la mettre le slash! juste le nom de la route
            return $this->redirectToRoute('profil');
        }

        return $this->render('registration/addplante.html.twig', [
            'addPlanteForm' => $form->createView(),
        ]);
    }
}
