<?php

namespace App\Controller;

use App\Repository\PieceRepository;
use App\Repository\PlanteRepository;
use App\Repository\RelationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var PlanteRepository
     */
    private $planteRepository;
    /**
     * @var PieceRepository
     */
    private $pieceRepository;
    /**
     * @var RelationRepository
     */
    private $relationRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, RelationRepository $relationRepository, PlanteRepository $planteRepository, PieceRepository $pieceRepository)
    {
        $this->entityManager =$entityManager;
        $this->userRepository = $userRepository;
        $this->relationRepository = $relationRepository;
        $this->planteRepository = $planteRepository;
        $this->pieceRepository = $pieceRepository;
    }
    
    /**
     * @Route("/home", name="home")
     */
    public function acceuil()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    public function register()
    {
        return $this->render('registration/register.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profil()
    {
        return $this->render('default/profil.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/", name="login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
